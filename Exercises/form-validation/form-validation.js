/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl
 * @param {Event} submitEvent
 */
const validateItem = function(inputEl, submitEvent) {
  const errorEl = inputEl.parentElement.querySelector('.error');

  if (inputEl.value.length < 5) {
    const labelEl = inputEl.parentElement.querySelector('label');
    errorEl.innerHTML = `${labelEl.innerText} is Required`;
    submitEvent.preventDefault();
    inputEl.parentElement.classList.add('invalid');
  } else {
    inputEl.parentElement.classList.remove('invalid');
  }
}

const validateEmail(inputEl, submitEvent) {
  const regex = new RegExp(/\w+@\w+\.\w+/);

  if (!regex.test(inputEl.value)) {
    const labelEl = inputEl.parentElement.querySelector('label');
    errorEl.innerHTML = `${labelEl.innerText} is Required`;
    submitEvent.preventDefault();
    inputEl.parentElement.classList.add('invalid');
  } else {
    inputEl.parentElement.classList.remove('invalid');
  }
}

const firstNameEl = document.getElementsByClassName('input')[0];
const lastNameEl = document.getElementsByClassName('input')[1];
const emailEl = document.getElementsByClassName('input')[2];


const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function(e) {
      validateText(firstNameEl, e);
      validateText(lastNameEl, e);
      validateEmail(emailEl, e);
    });
