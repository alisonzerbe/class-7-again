describe("Soccer", function() {

  it("Expect getTotalPoints to return correct total", function() {
    const points = soccer.getTotalPoints('wwdl');

    expect(points).toBe(7);
    expect(soccer.getTotalPoints('dddddl')).toBe(5);
    expect(soccer.getTotalPoints('wwwwdlll')).toBe(13);
  });
});
