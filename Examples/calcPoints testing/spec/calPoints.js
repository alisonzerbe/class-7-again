describe("calPoints", function() {
  const cardTen = {
    suit: 'Hearts',
    val: 10,
    displayVal: '10',
  };
  const cardSix = {
    suit: 'Hearts',
    val: 6,
    displayVal: '6',
  };

  const cardSeven = {
    suit: 'Hearts',
    val: 7,
    displayVal: '7',
  };

  const cardNine = {
    suit: 'Heartssss',
    val: 9,
    displayVal: '9',
  };

//does this start as a 10 or a 1?? or is it 11?
  const cardAce = {
    suit: 'Hearts',
    val: 1,
    displayVal: 'Ace',
  };

  it("should calculate the correct number of points", function() {
    const hand1 = [cardTen, cardSeven];
    const hand2 = [cardAce, cardNine];
    const hand3 = [cardTen, cardSix, cardAce];

    expect(determineScore(hand1).total).toEqual(17);
    expect(determineScore(hand2).total).toEqual(20);
    expect(determineScore(hand3).total).toEqual(17);
  });
});
