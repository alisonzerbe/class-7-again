// Kept the starter Jasmine Player Spec Code
// Update this!

describe("dealerShould function", function() {
  it("determines whether dealer should draw based on hand", function() {

    const cardTwo = {
      suit: 'Hearts',
      val: 2,
      displayVal: '2',
    };

    const cardFour = {
      suit: 'Hearts',
      val: 4,
      displayVal: '4',
    };

    const cardFive = {
      suit: 'Hearts',
      val: 5,
      displayVal: '5',
    };

    const cardSix = {
      suit: 'Hearts',
      val: 6,
      displayVal: '6',
    };

    const cardSeven = {
      suit: 'Hearts',
      val: 7,
      displayVal: '7',
    };

    const cardNine = {
      suit: 'Heartssss',
      val: 9,
      displayVal: '9',
    };
    const cardTen = {
      suit: 'Hearts',
      val: 10,
      displayVal: '10',
    };

    const cardAce = {
      suit: 'Hearts',
      val: 1,
      displayVal: 'Ace',
    };

    const hand1 = [cardTen, cardNine];
    const hand2 = [cardAce, cardSix];
    const hand3 =[cardTen, cardSeven];
    const hand4 = [cardTwo, cardFour,cardTwo, cardFive];

    expect(dealerShouldDraw(hand1)).toBeFalsy();
    expect(dealerShouldDraw(hand2)).toBeTruthy();
    expect(dealerShouldDraw(hand3)).toBeFalsy();
    expect(dealerShouldDraw(hand4)).toBeTruthy();
  });

});
