/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl
 * @param {Event} submitEvent
 */
const validateItem = function(inputEl, submitEvent) {
  const errorEl = inputEl.parentElement.querySelector('.error');

  // const value = inputEl.value;
  const formField = inputEl.parentElement.querySelector('label');
  // console.log('formField', formField.innerText);

//validates name
  if (formField.innerText === 'Name' && inputEl.value.length < 3) {
        errorEl.innerHTML = `${formField.innerText} is Required`;
        submitEvent.preventDefault();
  }

  //validates Email
  const regex = new RegExp(/\w+@\w+\.\w+/);
  if (formField.innerText === 'Email' && !regex.test(inputEl.value)) {
    errorEl.innerHTML = `${formField.innerText} is Required`;
    submitEvent.preventDefault();
  }

  //validates message
  if (formField.innerText === 'Message' && inputEl.value === '') {
    errorEl.innerHTML = `${formField.innerText} is Required`;
    submitEvent.preventDefault();
  }

}

const inputElements = document.getElementsByClassName('validate-input');

function toggleJobOpp() {
  document.getElementById('job-title').style.display = 'block';
}

function toggleLanguage() {
  document.getElementById('language').style.display = 'block';
}

const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function(e) {
      if (document.getElementById('job-opp').checked) {
        localStorage.setItem('contact_type', 'Job opportunity');
      }
      if (document.getElementById('talk-code').checked) {
        localStorage.setItem('contact_type', 'Talk code');
      }

      for (let i = 0; i < inputElements.length; i++) {
        validateItem(inputElements[i], e);
      }


      e.preventDefault();
    });
